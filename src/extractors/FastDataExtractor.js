module.exports = { createBatiment, enrichedWithBuildData, enrichedWithEmbellissementdData };

const tools = require("../tools")
const Building = require("../models/Building")
const AjaxRequest = require("./AjaxRequest")



async function createBatiment() {

    let htmlResult = await AjaxRequest.doAjaxRequest(`${process.env.IMMO_BASE_URL}/agency/agency_ajax.php`
        , "module=list&type=&min=0&max=50000000000000&available=false&search=1&sort=0&sortAsc=1")

    var localpage = await global.browser.newPage();
    await localpage.setContent(htmlResult)


    const batiments = []

    for (let row of await localpage.$$("tr.EITableBuilding")) {
        let id = await row.$eval("a", el => el.getAttribute("buildingid")).then(t => parseInt(t))
        let name = await row.$eval("a", el => el.innerText)
        let prix = await row.$eval("td:nth-child(2)", el => el.innerText).then(t => tools.priceToInt(t))
        let loyer = await row.$eval("td:nth-child(3)", el => el.innerText).then(t => tools.priceToInt(t))
        let charges = await row.$eval("td:nth-child(4)", el => el.innerText).then(t => tools.priceToInt(t))
        let impots = await row.$eval("td:nth-child(5)", el => el.innerText).then(t => tools.priceToInt(t))
        let dispo = await row.$eval("td:nth-child(6)", el => el.innerText).then(t => tools.priceToInt(t))

        batiments.push(new Building(id, name, prix, loyer, charges, impots, dispo))
    }

    await localpage.close()

    batiments.forEach(b => {
        b.isTerrain = b.isATerrain()
    })
    
    console.log("=== Loaded buildings ===")
    return batiments
}

async function enrichedWithBuildData(batiments) {
    let htmlResult = await AjaxRequest.doAjaxRequest(`${process.env.IMMO_BASE_URL}/properties/ajax.php`
        , "action=building&type=1&visibility=0&cid=0")

    var localpage = await global.browser.newPage();
    await localpage.setContent(htmlResult)

    const result = []

    for (let row of await localpage.$$("#tblBuildings tbody tr")) {
        let batName = await row.$eval("td:nth-child(2)", el => el.innerText)
        let terrainName = await row.$eval("td:nth-child(3)", el => el.innerText)
        let cost = await row.$eval("td:nth-child(4)", el => el.innerText).then(t => tools.priceToInt(t))
        let time = await row.$eval("td:nth-child(5)", el => el.innerText).then(t => tools.priceToInt(t))
        let type = await row.$eval("td:nth-child(1)", el => el.innerText)

        let target = batiments.find(b => b.name === batName);
        target.buildPrice = cost
        target.buildTime = time
        target.type = type
        if (terrainName) target.terrainId = batiments.find(b => b.name === terrainName).id
    }


    await localpage.close()
    console.log("=== Loaded buildings build details ===")
}


async function enrichedWithEmbellissementdData(batiments) {
    let htmlResult = await AjaxRequest.doAjaxRequest(`${process.env.IMMO_BASE_URL}/properties/ajax.php`
        , "action=building&type=2&visibility=0&cid=0")

    var localpage = await global.browser.newPage();
    await localpage.setContent(htmlResult)

    const result = []

    for (let row of await localpage.$$("#tblBuildings tbody tr")) {
        let batName = await row.$eval("td:nth-child(2)", el => el.innerText)
        let terrainName = await row.$eval("td:nth-child(3)", el => el.innerText)
        let cost = await row.$eval("td:nth-child(4)", el => el.innerText).then(t => tools.priceToInt(t))
        let time = await row.$eval("td:nth-child(5)", el => el.innerText).then(t => tools.priceToInt(t))
        let type = await row.$eval("td:nth-child(1)", el => el.innerText)

        let target = batiments.find(b => b.name === batName);
        target.upgrapdePrice = cost
        target.upgrapdeTime = time
        target.type = type
        if (terrainName) target.terrainId = batiments.find(b => b.name === terrainName).id
    }

    await localpage.close()    
    console.log("=== Loaded buildings embellissement details ===")
}





