module.exports = {
    saveCurrentData, getPromoteurHistoryLight,
    getStockInfoByBatimentId, getBoughtQuantityForBatidByDay,
    getBatimentPriceById
};

const buildingService = require("../dataServices/BuildingService")
const metadataService = require("../dataServices/MetadataService")
const mongodbService = require("./MongodbService")

const moment = require("moment")
const _ = require('lodash');

async function saveCurrentData() {

    let time = Math.round(Date.now() / 1000)

    //We comptact data to avoid using space
    let buildings = buildingService.getBuidlings().map((b) => {
        return {
            time: time,
            id: b.id,
            prix: b.prix,
            loyer: b.loyer,
            dispo: b.dispo,
        }
    });

    let metadata = {
        ...metadataService.getMetadata(),
        time: time,
    };


    await mongodbService.getMetadataCollection().insertOne(metadata)
    await mongodbService.getBuidlingCollection().insertMany(buildings)

    console.log("=== Data saved to mongo ===")
}

async function getPromoteurHistoryLight() {
    let col = mongodbService.getMetadataCollection()
    let fatResult = await col.find({}).toArray()
    let result = []

    fatResult.forEach(element => {

        if (result.at(-1)?.promoteurPercent !== element.promoteurPercent) {
            result.push({
                promoteurPercent: element.promoteurPercent,
                time: element.time
            })
        }
    });


    return result
}

/**
 * Traitement un peu spécial, le but est de retourner par groupe de
 * 15 minutes les data que l'on a. Puisque toutes les 15 mn
 * les terrains sont rechargés. A la fin des 15 min on peut en 
 * déduire les quantités achetés.
 * Due à l'inconsistance des données on prend le premier resultat des 15 minutes
 * puis ensuite le dernier.
 * @param {*} batid 
 * @returns 
 */
async function getStockInfoByBatimentId(batid) {

    let researched = await mongodbService.getBuidlingCollection().find({ id: batid }).project({ _id: 0, time: 1, dispo: 1 }).toArray()

    researched = researched.map(d => {
        let quartHeureRaw = Math.floor(moment(d.time * 1000).minute() / 15)
        let quartHeureRefSecond = moment(d.time * 1000).seconds(0).millisecond(0).minute(quartHeureRaw)

        return {
            ...d,
            quartHeureRef: quartHeureRefSecond.unix()
        }
    })

    researched = researched.reduce((groups, item) => {
        const group = (groups[item.quartHeureRef] || []);
        group.push(item);
        groups[item.quartHeureRef] = group;
        return groups;
    }, {});


    //We only keep first and last values, intermediate are useless
    researched = Object.entries(researched).map(([key, value]) => {
        return {
            quartHeureTS: parseInt(key),
            data: [value[0], value.at(-1)]
        }
    })


    return researched;
}

/**
 * Calcul le nombre de vente par rapport aux nombre de batiments dispo
 * @param {*} batid 
 * @returns 
 */
async function getBoughtQuantityForBatidByDay(batid) {
    let groupedDispo = await getStockInfoByBatimentId(batid)
    groupedDispo = groupedDispo.map(m => {
        let dayRef = moment(m.quartHeureTS * 1000).millisecond(0).seconds(0).minute(0).hour(0)
        return {
            day: dayRef.unix(),
            bought: (m.data[0].dispo - m.data[1].dispo)
        }
    })
    groupedDispo = _.groupBy(groupedDispo, "day")
    groupedDispo = Object.entries(groupedDispo).map(([key, value]) => {
        return {
            quantityBought: _.reduce(value, (sum, n) => { return sum + n.bought; }, 0),
            dayTs: parseInt(key)
        }
    })
    return groupedDispo
}

async function getBatimentPriceById(batid) {

    let researched = await mongodbService.getBuidlingCollection().find({ id: batid }).project({ _id: 0, time: 1, prix: 1, loyer: 1 }).toArray()

    researched = researched.map(m => {
        let week = moment(m.time * 1000).week()
        return {
            ...m,
            week: week
        }
    })

    researched = _.groupBy(researched, "week")

    researched = Object.entries(researched).map(([key, value]) => {
        return value.at(-1)
    })

    return researched;
}