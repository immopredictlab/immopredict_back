module.exports = { initMongodb, getMetadataCollection, getBuidlingCollection };

const MongoClient = require('mongodb').MongoClient;

var database
var buidlingCollection
var metadataCollection

async function initMongodb() {

    const uri = `mongodb://${process.env.IMMO_MONGODB_USERNAME}:${process.env.IMMO_MONGODB_PASSWORD}@${process.env.IMMO_MONGODB_URL}`
    await MongoClient.connect(uri, { useNewUrlParser: true })
        .then(async (client) => {
            database = await client.db(process.env.IMMO_MONGODB_DBNAME)
            buidlingCollection = await database.collection("buildings")
            metadataCollection = await database.collection("metadata")
            console.log("=== MongoDB connected ====");
        }).catch(err => {
            console.log("Failed to conect to mongodb", err);
            throw new Error(err)
        })
}

function getMetadataCollection() {
    return metadataCollection
}

function getBuidlingCollection() {
    return buidlingCollection
}
