module.exports = class Building {

    constructor(id, name, prix, loyer, charges, impots, dispo) {
        this.id = id;
        this.name = name;
        this.prix = prix;
        this.loyer = loyer;
        this.charges = charges;
        this.impots = impots;
        this.dispo = dispo;
        this.type = null;
        this.terrainId = null;
        this.buildPrice = null;
        this.buildTime = null;
        this.upgrapdePrice = null;
        this.upgrapdeTime = null;

        this.nextBuildingId = null;
        this.previousBuildingId = null;

        this.isTerrain = null;

    }


    getPos() {
        let Appartement = [["non-aménagé"], ["habitable"], ["équipé"], ["lux"]]
        let Chambre = [["vides", "non-aménagée"], ["habitable"], ["équipé"], ["tout confort"]]
        let Complexe = [["non-aménagé"], ["habitable"], ["tout confort"], ["luxue"]]
        let Maison = [["abandonnée"], ["habitable"], ["équipé"], ["luxue"]]

        let selected = null
        switch (true) {
            case this.type == "Appartement": selected = Appartement; break;
            case this.type == "Chambre": selected = Chambre; break;
            case this.type == "Complexe": selected = Complexe; break;
            case this.type == "Maison": selected = Maison; break;
            default: {
                console.log(this)
                throw new Error(`Eror while get pos, unknow type:${this.type} id:${this.id} name:${this.name}`)
            }
        }

        let index = selected.findIndex(sublist => sublist.some(n => this.name.includes(n)))

        if (index == undefined || index == -1) {
            console.log(this)
            throw new Error("Failed to get position")
        }
        return index + 1
    }

    getNext() {
        let pos = this.getPos()
        if (pos == 4) return null
        return this.id + 1
    }

    getPrevious() {
        let pos = this.getPos()
        if (pos == 1) return null
        return this.id - 1
    }

    isATerrain() {
        return [1, 2, 3, 4, 173].some(curr => curr == this.id)
    }

}
