require('dotenv').config()

const puppeteer = require('puppeteer');
const authentication = require("./service/authentication/AuthenticationService");
const brain = require("./service/Brain");
const mongodbService = require("./service/datastorage/MongodbService")



async function start() {
    await mongodbService.initMongodb()
    global.browser = await puppeteer.launch({ headless: process.env.IMMO_HEADLESS === 'true', defaultViewport: null, args: ['--no-sandbox'] });
    global.page = await global.browser.newPage();
    await authentication.doAuth()
    await brain.start()
  
}

start()